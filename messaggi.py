# -*- coding: UTF-8 -*-

from random import choice
from parsing import beta


def umano(testo):
	if any(i in testo.lower() for i in ("grazie", "thx", "thank", "ty", "top", "grz", "scusa", "sks")):
		return choice([
			"Figurati.", "Non c'è di che.", "Non c'è di cocco.",
			"Di nulla.", "Faccio solo il mio lavoro.", "Non ti preoccupare.",
			"Usa e fai usare questo bot."])
	elif "busfinder" in testo.lower() or ("bus" in testo.lower() and "finder" in testo.lower()):
		return choice(["Non paragonarmi a quel coso.", "Ma chi lo usa quella cosa?", "Forse intendevi 'bugfinder'."])
	elif "?" in testo[-1]:
		return choice(["Me lo chiedo anch'io.", "Non so cosa dirti.", "Non chiedere a me"])
	elif any(i in testo.lower() for i in ("grande", "bell", "gentil")):
		return choice(["Mi fai diventare rosso.", "Oh grazie!", "Dovrebbero dirmelo piu' spesso..."])
	elif any(i in testo.lower() for i in ("ritardo", "noo")):
		return choice(["Sono cose che possono capitare.", "Non e' colpa mia.", "Capita.", "Succede."])
	elif any(i in testo.lower() for i in (
		"lol", "lool", "loool", "xd", "ahah", "rido", "muoio", "mi sento male", "morto", "morta")):
		return choice([
			"Beh almeno te la prendi a ridere.", "Ridi ridi che mamma ha fatto gli gnocchi.",
			"Esagerato"])
	elif any(i in testo.lower() for i in ("frat", " zio", "zio ",  "nenno")):
		return choice(["Grande, zio", "Bella zio, come ti butta?", "yo"])
	elif any(i in testo.lower() for i in ("ciao", "buongiorno", "buonasera", "we", "hey")):
		return choice(["Ciao anche a te.", "Oh, ciao!", "Salve.", "Heila!"])
	elif "the game" in testo.lower():
		return "Ho perso"
	elif "ho perso" in testo.lower():
		return "Anch'io."
	else:
		return testo


def minuti(chat_id):
	try:
		f = open("set" + str(chat_id) + ".txt")
	except IOError:
		m = "2"
	else:
		print "Ho aperto i minuti"
		m = f.read()
		print m
		f.close()
	messaggio = "Ti manderò un messaggio appena mi accorgo che "
	if m == "1":
		messaggio += "manca meno di un minuto "
	else:
		messaggio += "mancano meno di " + numeri(m) + " minuti "
	messaggio += "al passaggio.\nCambia tempo di preavviso con il comando /set + minuti di preavviso."
	return messaggio


def attesa():
	casuale = [
		"Attendi il caricamento degli orari.",
		"Hai mai provato la ricerca vocale?",
		"Non trovi una fermata? Prova a scrivere 'Linea'.",
		"Sto lavorando per te.",
		"Aspetta poco poco.",
		"Si prega di attendere.",
		# "Se la CTM mi risponde, te lo dico.",
		"Attendez-vous svp.",
		"Bitte warten.",
		"Please wait.",
		"Aspé che te lo dico subito.",
		"Scusami ero distratto, ora cerco gli orari.",
		"Attendi.",
		"Sai, se rimani nella chat ti aggiorno in tempo reale.",
		"Ricordati di obbliterare quando sali sul bus.",
		"Se non guidi puoi bere irresponsabilmente.",
		"Ora arrivano gli orari, aspetta",
		"Caricamento.",
		"Un attimino."
		"Controllo subito.",
		"Ci penso io.",
		"Benvenuto.",
		"Io sono Ironman",
		"Sono Batman",
		"Massimo sforzo",
		"Un attimo solo",
		"Dammi solo un minuto.",
		"Un attimo e arrivo",
		"L'attesa del bus è essa stessa il bus.",
		"Torno subito.",
		"Vado, leggo gli orari e torno.",
		"Aspetta aspetta",
		"Wait, wait.",
		"Ora te li esco.",
		"Ora ti dico i tasinanta.",
		"Massimo sforzo.",
		"It's a-me, Mario",
		"Iscriviti al canale @cagliaribotnews per essere aggiornato su spostamenti fermate, "
		"rallentamenti delle linee e altro.",
		"Se mi mandi la posizione ti dico le fermate più vicine.",
		# "Scusi, sa a che ora passa il pullman?\nSono il bot del CTM, veda un po' lei.",
		"Che cos'è la vita, se non il tempo che intercorre tra un bus e un altro.",
		"Mens sana in bus sano.",
		"Donde está la biblioteca?",
		"Non trovi più il biglietto? Prova nel \"portafogli da galera\".",
		"Un omomorfismo bigettivo si dice isomorfismo.",
		"Sei il milionesimo visitatore, questo non Ä' uno scherzo!",
		"Sono veramente euforico.",
		"Le informazioni pregresse sono una base incerta.",
		"La voglia scalpitava, strillava, tuonava cantava da notte fonda nel petto di",
		"È la tua risposta definitiva?",
		"Hier Das Erste Deutsche Fernsehe, mit der Tagesschau.",
		"The Netherlands twelve points, le Pays Bas douze points.",
		"Dirige l'orchestra Beppe Vessicchio.",
		"Su le mani dai divani, su le mani per Osmany.",
		"Il puntino luminoso al centro sei tu, Snake.",
		"Credi nel cuore delle ProxyCard.",
		"Get ready for the next bus.",
		"Device connected.",
		# "Il sistema ha rilevato un errore interno e Windows è stato arrestato",
		"Ce li hai due euro?",
		"Siamo figli di Pitagora e di Casadei,\ndi Machiavelli e di Totò",
		"Ti ricordo che questo bot non è ufficiale e che non ha nulla a che vedere con l'azienda"
	]
	return choice(casuale)


def sql_controllo(txt):
	if "cascade" in txt.lower() or \
			"--" in txt or "drop" in txt.lower() or "table" in txt.lower() or \
			"=" in txt or "true" in txt.lower() or "alter" in txt.lower() or "select" in txt.lower():
		return True
	else:
		return False


def numeri(num):
	lett = {2: "due", 3: "tre", 4: "quattro", 5: "cinque", 6: "sei", 7: "sette", 8: "otto", 9: "nove"}
	try:
		return lett[int(num)]
	except KeyError:
		return str(num)


nonvalido = ["Non valido.", []]
nonhocapito = ["Non ho capito quello che hai detto, mi dispiace.", []]
path = "/home/ubuntu/ctm/"
sql = ["Se stai cercando di fare una SQL injection, cadi male.", []]
slash = ["Per cercare una fermata devi scrivere il nome della fermata, con gli spazi e senza / iniziale.", []]
saluto = ["""
Ciao. Questo bot ti permetterà di cercare gli orari degli autobus. Scrivi il nome della fermata per sapere gli orari.
_Questo bot non è ufficiale ma un progetto di uno studente universitario._
(Premi /help per scopire tutte le altre funzioni)""", []]
server = "_Sono stati riscontati problemi di connessione con i server. Scusa il ritardo._\n"
nonmodificato = \
	"(u'Bad Request: message is not modified', 400, {u'error_code': 400, u'ok': False," \
	" u'description': u'Bad Request: message is not modified'})"
nontrovato = \
	"(u'Bad Request: message to delete not found', 400, {u'error_code': 400, u'ok': False," \
	" u'description': u'Bad Request: message to delete not found'})"
set_errato = [
	"Devi scrivere /set seguito dal numero di minuti di preavviso.\nEsempio: /set 5\n"
	"(Suggerimento: tieni premuto /set)", []]
aspettando = "Cosa stai aspettando?\nDisattiva questa funzione con /passing.\n[Linea] Previsione di passaggio"
pulsanti_aiuto = ["Avanti", "Indietro"]
posizione = "\n\nPotresti provare a mandarmi la posizione per trovare le fermate più vicine a te."
troppefermate = "Sono state trovate troppe fermate. Raffina la ricerca.\nSono mostrate solo alcune."
fermatevicine = "Mostra le fermate vicine"

linee = [
	["Linea 1"],
	["Linea 3"],
	["Linea M"],
	["Linea 5"],
	["Linea 5-11"],
	["Linea 6"],
	["Linea 7"],
	["Linea 8"],
	["Linea 8A"],
	["Linea 9"],
	["Linea 10"],
	["Linea 11"],
	["Linea 13"],
	["Linea 15"],
	["Linea 16"],
	["Linea 17"],
	["Linea 18"],
	["Linea 19"],
	["Linea 20"],
	["Linea 29"],
	["Linea 30"],
	["Linea 30R"],
	["Linea 31"],
	["Linea 31R"],
	["Linea 40"],
	["Linea 41"],
	["Linea 1Q"],
	["Linea PF"],
	["Linea PQ"],
	["Linea QS"],
	["Linea QSA"],
	["Linea QSB"],
	["Linea CEP"],
	["Linea ZONA INDUSTRIALE"],
	["Linea U-EX"],
	["Linea GSS"]
]

cerca_linea = ["Per cercare una linea, scrivi \"Linea\" o \"Circolare\" seguito dalla linea interessata.", linee]
fuoristagione = "Mi spiace, la linea cercata è fuori stagione o soppressa."


video = [
	"Ma come sei carin*!",
	"Perché quella faccia?",
	"Ti vedo stanco...",
	"È sempre bello vederti!",
	"Che faccia!",
	"Ma dove vai conciat* così?",
	"Olioh"
]

with open(beta("path") + "aiuto.txt") as h:
	aiuto = eval(h.read())
