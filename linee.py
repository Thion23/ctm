import re
import urllib


def correggiascii(testo):
	corretto = ""
	for i in testo:
		if 0 <= ord(i) <= 127:
			corretto += i
	return corretto


def find_between(s, first, last=""):
	try:
		start = s.index(first) + len(first)
		if last == "":
			return s[start:]
		else:
			end = s.index(last, start)
			return s[start:end]
	except ValueError:
		return ""


def direzioni():
	page = urllib.urlopen("http://m.ctmcagliari.it/linee.php")
	sorgente = page.read()
	linee = re.findall("verso.php[?]id=" + cod + ";[A-z 0-9]{2,4};[AsDi]{2}\'>[A-z .0-9-']*", sorgente)
	direzione = {}

	for i in linee:
		linea = find_between(i, ";", "'")
		direz = find_between(i, "- ", "")
		direzione[linea] = direz

	return direzione


def percorso(direzione, flag=False):  # Data una linea con direzione, restituisce una lista dei numeri delle fermate
	# verso.php?id=401 in inverno, 405 in estate
	page = urllib.urlopen("http://m.ctmcagliari.it/verso.php?id=" + cod + ";" + direzione)
	fermate_app = re.findall(reg(flag), page.read())
	fermate = []
	for i in fermate_app:
		if flag:
			i = i.replace("'", "")
			i = int(i)
		else:
			i = [correggiascii(i.replace("\t", ""))]
		try:
			fermate.append(i)
		except Exception as e:
			print "Percorso: ", str(e)
	return fermate


def reg(flag=False):
	if not flag:
		return "\t\t\t\t\t.*\t\t\t\t"
	else:
		return "\\d{4}\'"


def fermata(numero):
	f = open("paline.txt")  # beta
	# f = open("/home/ubuntu/ctm/paline.txt", "r")
	num = str(numero)
	print num, len(num)
	while len(num) <= 4:
		print num
		num = "0" + num
	tupla = re.findall(".*\',\'" + num + "\'[)]", f.read())
	print tupla
	f.close()
	return eval(tupla[0])


def regex(linea):
	return True if re.findall("(?i)^linea.*|circolare.*", linea) else False


pagina = urllib.urlopen("http://m.ctmcagliari.it/linee.php")
cod = find_between(pagina.read(), "verso.php?id=", ";")

LINEA = {
	"1": "01",
	"3": "03",
	"3P": "03P",
	"M": "00M",
	"SAN SIMONE": "0SS",
	"SS": "0SS",
	"5": "05",
	"5ZEUS": "5_ZE",
	"5ZEEUS": "5_ZE",
	"5 ZEUS": "5_ZE",
	"5 ZEEUS": "5_ZE",
	"5ZE": "5_ZE",
	"5_ZE": "5_ZE",
	"5-ZE": "5_ZE",
	"5-11": "511",
	"511": "511",
	"6": "06",
	"7": "07",
	"8": "08",
	"GIORGINO": "08G",
	"8A": "08A",
	"9": "09",
	"10": "10",
	"11": "11",
	"13": "13",
	"15": "15",
	"16": "16",
	"17": "17",
	"18": "18",
	"19": "19",
	"20": "20",
	"29": "29",
	"30": "30",
	"30R": "30R",
	"31": "31",
	"31R": "31R",
	"40": "40",
	"41": "41",
	"1Q": "01Q",
	"PF": "60F",
	"PQ": "60Q",
	"QS": "50",
	"QSA": "50A",
	"QSB": "50B",
	"CEP": "00C",
	"C": "00C",
	"ZONA INDUSTRIALE": "00Z",
	"UNIVERSITY EXPRESS": "U_EX",
	"University Express": "U_EX",
	"U_EX": "U_EX",
	"U-EX": "U_EX",
	"P-E": "P_E",
	"P_E": "P_E",
	"POETTO EXPRESS": "P_E",
	"NN": "NN",
	"Navetta Natale": "NN",
	"NATALE": "NN",
	"L": "00L",
	"IN": "0IN",
	"IR": "0IR",
	"EN": "0EN",
	"ES": "0ES",
	"INTERNA NERA": "0IN",
	"INTERNA ROSSA": "0IR",
	"ESTERNA NERA": "0EN",
	"ESTERNA ROSSA": "0ER",
	"GSS": "GSS",
	"Giorgino - San Simone": "GSS",
	"NSB": "NSB",
	"Navetta San Benedetto": "NSB",
	"san benedetto": "NSB",
	"BN": "0NB",
	"SAN BENEDETTO": "NSB",
	"BLU NOTTE": "0NB",
	"NB": "0NB"
}
