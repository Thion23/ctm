(
SELECT chatid FROM ctm.log WHERE giorno::date = 'today' GROUP BY chatid
)
except
(
SELECT chatid FROM ctm.log WHERE giorno::date = 'yesterday' GROUP BY chatid
)