# -*- coding: UTF-8 -*-
"""
Utilizzare un database al posto delle tabelle hash sarebbe stato più pratico e funzionale,
ma la connessione non sempre è stabile e causerebbe non pochi problemi nel caso cadesse.
"""

import string
import re
import datetime
import requests
import linee
from urllib import urlopen
from pytz import timezone


def beta(restituisci):
	flag = 0  # 0: beta off, 1: beta on
	if flag:  # Beta on
		if restituisci == "path":
			return ""
		elif restituisci == "canale":
			# return -1001190461718
			return 241547230
		elif restituisci == "token":
			with open("/home/thion/token") as f:
				token = f.read()
			return token
	else:
		if restituisci == "path":
			return "/home/ubuntu/ctm/"
		elif restituisci == "canale":
			return -1001190461718
		elif restituisci == "token":
			with open("/home/ubuntu/token") as f:
				token = f.read()
			return token


def fermata():
	f = open(beta("path") + "paline.txt")
	# f = open(beta("path") + "sql.txt")
	bstop = {}
	while 1:
		linea = string.lower(f.readline())
		if linea == "":
			f.close()
			return bstop
		else:
			tupla = eval(linea)
			bstop[correggiascii(tupla[0])] = tupla[1]


def cerca(codice):
	orari = eval(codice.read().replace("null", "None"))
	# print orari
	if orari['header']['statusCode'] == "SERVICE_UNAVAILABLE" or orari['header']['statusCode'] == 'INTERNAL_SERVER_ERROR':
		if orari['contents'][0]['message'] == "Errore: fermata 'None' non trovata":
			return ["Nessuna fermata trovata.", []]
		elif orari['contents'][0]['message'] == 'Read timed out':
			return ["La connessione ha impiegato troppo tempo. Riprova", []]
		else:
			return ["Servizio temporaneamente non disponibile. Riprova.", [codice.read()]]
	else:
		ferm = orari['header']['request']['params'][0]['value']
		print "Fermata", ferm
		messaggio = "*" + codstop[int(ferm[2:])] + "*\n\n"
		for linea in orari['contents']:
			messaggio += "Linea " + linea['lineCode'].replace("_", "-") + " "
			try:
				messaggio += \
					"_verso " + direz(int(orari['header']['request']['params'][0]['value'][2:]), linea['lineCode']) + "_"
				d = direz(ferm[2:], linea['lineCode'], True)
				corse = corse_limitate(ferm, d)
				print corse
			except Exception as e:
				d, corse = "", ""
				print "Direzione errata: ", str(e)
			messaggio += "\n"
			flag = ""
			limitata = ""
			for i in range(min(len(linea['transits']), 4)):
				orario = linea['transits'][i]
				if orario['type'] == "PRED":
					messaggio += "`" + str(ora(orario['time'] * 0.001)) + "`"
				else:
					messaggio += "_" + str(ora(orario['time'] * 0.001)) + "_"
				if corse:
					limitata = controllolimitato(corse, str(ora(orario['time'] * 0.001)))
					print limitata
				if limitata:
					messaggio += "*L*"
					flag = str(limitata)
				messaggio += "   "
			print limitata
			if flag:
				messaggio += "\n*[L]imitata " + flag + "*"
			messaggio += "\n\n"
	messaggio += "Gli orari in corsivo sono teorici, pertanto non affidabili.\n"
	if "[L]" in messaggio:
		messaggio += "*Nota bene!* Le corse limitate sono da intendersi teoriche.\n"
	messaggio += "Numero fermata: " + orari['header']['request']['params'][0]['value'][2:] + "."
	return [messaggio, []]


def ora(tempo):
	return datetime.datetime.fromtimestamp(tempo, tz=rome).strftime("%H:%M")


def converti(s):
	secondi = "0" + str(s % 60)
	return str(s / 60) + ":" + secondi[-2:]


def esplodi(stringa, carattere=" "):  # prende in ingresso una stringa e un separatore e restituisce un array di parole
	array = []  # inizializza l'array
	parola = ""  # inizializza la stringa d'appoggio
	for i in range(len(stringa)):  # scorre la stringa
		if stringa[i] == carattere or i == len(stringa) - 1:  # se ha raggiunto la fine
			if stringa[i] != carattere:  # se e' l'ultima lettera
				parola += stringa[i]  # aggiunge l'ultima lettera
			array.append(parola)  # aggiunge la parola all'array
			parola = ""  # azzera la stringa d'appoggio per la nuova parola
		else:  # altrimenti
			parola += stringa[i]  # aggiunge la lettera alla parola
	return array  # ritorna l'array


def find_between(s, first, last):
	try:
		start = s.index(first) + len(first)
		end = s.index(last, start)
		return s[start:end]
	except ValueError:
		return ""


def stop(text):
	if re.findall("[A-z]{2}[0-9]{4}", text):
		return text.upper()
	try:
		return busstop[int(text)]
	except (KeyError, ValueError):
		word = esplodi(text, " ")
		word = toglivia(word)
		print word
		f = parsstop(word, fermate.keys(), text)
		if len(f) == 1:
			return busstop[int(fermate[f[0].lower()])]
		elif len(f) == 0:
			return "None"
		else:
			lista = []
			for i in f:
				lista.append([primamaiuscola(i)])
			return lista


def toglivia(word):
	for i in ["via", "piazza", "viale", "corso"]:
		try:
			word.remove(i)
		except ValueError:
			pass
	return word


def parsstop(lista, stops, nome, i=0):
	if i == len(lista) or len(stops) == 1:
		return stops
	else:
		bstop = []
		for f in stops:
			if correggiascii(lista[i].lower()) in f:
				if nome.lower() == f:
					return [f]
				else:
					bstop.append(f)
		return parsstop(lista, bstop, nome, i+1)


def primamaiuscola(testo):
	stringa = ""
	for i in range(len(testo)):
		if i == 0:
			stringa += testo[i].upper()
		elif testo[i - 1] == " " or testo[i - 1] == "." or testo[i - 1] == "-" or testo[i - 1] == "'" or testo[i-1] == "(":
			stringa += testo[i].upper()
		else:
			stringa += testo[i]
	stringa = stringa.replace("Ang.", "ang.")
	stringa = stringa.replace("Via", "via")
	return stringa


def stampaaschermo(testo):
	stamp = "LINEA-IN ARRIVO"
	righe = esplodi(testo, "\n")
	# print righe
	for i in righe:
		if "Previsioni" in i:
			pass
		elif "verso" in i:
			stamp += find_between(i, "Linea ", " verso")
		elif ":" in i:
			stamp += "\t  " + i[0:5]
		elif i == "":
			stamp += "\n"
	print stamp


def correggiurl(testo):
	corretto = ""
	for i in testo:
		if " " == i:
			corretto += "+"
		elif 0 <= ord(i) <= 127 and ord(i) != 41:
			corretto += i
	return corretto


def correggiascii(testo):
	corretto = ""
	for i in testo:
		if 0 <= ord(i) <= 127:
			corretto += i
	return corretto


def correzioni(txt):
	testo = txt.lower()
	testo = testo.replace("angolo", "ang")  # Correzioni automatiche del bot
	testo = testo.replace("baccaredda", "bacaredda")
	testo = testo.replace("  ", " ")
	testo = testo.replace("ventitreesimo", "xxiii")
	testo = testo.replace("ugo foscolo", "foscolo")
	testo = testo.replace("montegrappa", "monte grappa")
	testo = testo.replace("frasche", "frasc")
	testo = testo.replace("metro", "metr")
	testo = testo.replace("sanita", "sanit")
	return testo


def quantomanca(richiesta):
	json = requests.get(ctm + busstop[int(richiesta[0])] + ctmf)
	ferm = json.json()
	tim = 0
	now = datetime.datetime.now(tz=rome)
	try:
		for linea in ferm['contents']:
			if linea['lineCode'].replace("_", "-") == richiesta[1]:
				for orario in linea['transits']:
					if orario['type'] == "PRED":
						tim = datetime.datetime.fromtimestamp(orario['time'] * 0.001, tz=rome)
						break
				break
	except KeyError:
		pass
	if tim:
		return tim.strftime("%H:%M"), ((tim - now).seconds + 1) / 60
	else:
		return 0


def spaziinutili(testo):
	while "  " in testo:
		testo = testo.replace("  ", " ")


def separa_messaggio(risposta):
	bstop = [find_between(risposta, "fermata: ", ".")]
	righe = esplodi(risposta, "\n")
	for i in range(len(righe) - 1):
		if "Linea" in righe[i]:
			bstop.append((find_between(righe[i], "Linea ", " "), find_between(righe[i+1], "`", "`")))
	return bstop


def direz(ferm, linea, flag=False):  # Questo metodo serve per capire in quale direzione è la fermata richiesta
	# Le linee hanno due direzioni: "As" è andata e "Di" è ritorno
	# Le circolari invece hanno una sola direzione "As"
	try:
		line = linee.LINEA[linea]
	except (IndexError, ValueError):
		line = linea
	if int(ferm) in linee.percorso(line + ";As", True):
		if flag:
			return line + ";As"
		else:
			return direzioni[line + ";As"]
	else:
		if flag:
			return line + ";Di"
		else:
			return direzioni[line + ";Di"]


"""
def linee(codice, txt):
	sorgente = codice.read()
	linea = re.findall("(?i)\tlinea " + txt + "\t|\tcircolare " + txt + "\t|\tnavetta " + txt + "\t", sorgente)
	print linea
"""


def iterative_levenshtein(s, t, **weight_dict):
	"""
		iterative_levenshtein(s, t) -> ldist
		ldist is the Levenshtein distance between the strings
		s and t.
		For all i and j, dist[i,j] will contain the Levenshtein
		distance between the first i characters of s and the
		first j characters of t
		weight_dict: keyword parameters setting the costs for characters,
		the default value for a character will be 1
	"""
	rows = len(s) + 1
	cols = len(t) + 1

	alphabet = "abcdefghijklmnopqrstuvwxyz 0123456789.'()-/"
	w = dict((x, (1, 1, 1)) for x in alphabet + alphabet.upper())
	if weight_dict:
		w.update(weight_dict)

	dist = [[0*i*j for i in range(cols)] for j in range(rows)]  # 0*i*j=0, serve per evitare "Variabile non usata"
	# source prefixes can be transformed into empty strings
	# by deletions:
	row, col = 0, 0
	for row in range(1, rows):
		dist[row][0] = dist[row - 1][0] + w[s[row - 1]][0]
	# target prefixes can be created from an empty source string
	# by inserting the characters
	for col in range(1, cols):
		dist[0][col] = dist[0][col - 1] + w[t[col - 1]][1]

	for col in range(1, cols):
		for row in range(1, rows):
			deletes = w[s[row - 1]][0]
			inserts = w[t[col - 1]][1]
			subs = max((w[s[row - 1]][2], w[t[col - 1]][2]))
			if s[row - 1] == t[col - 1]:
				subs = 0
			else:
				subs = subs
			dist[row][col] =\
				min(
					dist[row - 1][col] + deletes,
					dist[row][col - 1] + inserts,
					dist[row - 1][col - 1] + subs)  # substitution
	return dist[row][col]


def bsnum():  # bus stop number
	with open(beta("path") + "sql.txt") as f:
		bs = eval(f.read())
	return bs


def inverti():
	with open(beta("path") + "paline.txt") as f:
		linea = f.readline()
		codice = {}
		while linea != "":
			riga = eval(linea)
			codice[int(riga[1])] = riga[0]
			linea = f.readline()
		return codice


def dividilista(lista):
	ritorna = []
	for i in range(len(lista)):
		if i % 2 == 0:
			ritorna.append(lista[i])
		else:
			ritorna[i / 2].append(lista[i][0])
	return ritorna


def umano(text):
	return True if "Nessuna fermata" in text or "Sono state" in text else False


def forse_cercavi(text):
	tastiera = []
	simili = []
	try:
		for i in parole:
			distanza = iterative_levenshtein(i, text)
			if 0 < distanza < len(text) / 2 + 1:
				simili.append([distanza, i.upper()])
			tastiera = rilista(riordina(simili))
	except Exception as e:
		print e
	return tastiera


def rilista(lista):
	relist = []
	for i in lista:
		relist.append([i[1]])
	return relist


def riordina(lista):
	for i in range(len(lista)):
		k = i
		for j in range(i + 1, len(lista)):
			if lista[k][0] > lista[j][0]:
				k = j
		lista[k], lista[i] = lista[i], lista[k]
	return lista


def passoff(chatid):
	global passing
	if chatid in passing:
		passing.remove(chatid)
		v = True
	else:
		passing.append(chatid)
		v = False
	with open(beta("path") + "passing.txt", "w") as ps:
		ps.write(str(passing))
	return v


def corse_limitate(ferm, linea):
	tabella = urlopen(tab_teorico(ferm, linea))
	now = [datetime.datetime.now(tz=rome).strftime("%H")]
	tabella = tabella.readlines()
	ore = []
	minuti = []
	for riga in range(len(tabella)):
		if "<td class='ore'>" + str(now[0]) in tabella[riga] or "<td class='ore'>" + str(int(now[0]) + 1) in tabella[riga]:
			print tabella[riga + 1]
			ore.append([find_between(tabella[riga], "<td class='ore'>", "<") + ":"])
			ore.append(re.findall('[>0-9]*[*][<]', tabella[riga + 1]))
			if len(minuti) == 0:
				minuti = [find_between(tabella[riga + 1], "title='Questa corsa termina alla fermata ", "'")]
	h = "0"
	for i in ore:
		flag = 0
		if type(i) == list:
			for j in i:
				if ":" in j:
					if h > str(j):
						flag = 1
						break
					h = str(j)
				if "*" in j:
					minuti.append(h + j[1:3])
			if flag:
				break
	return minuti


def tab_teorico(ferm, linea):
	ctmt = "http://m.ctmcagliari.it/orari_fermata.php?verso="
	ctmt += linee.cod + ";"
	ctmt += linea
	ctmt += "&palina=" + linee.cod + ";" + ferm
	return ctmt


def controllolimitato(corse, hour, timedelta=4):
	try:
		limitata = corse[0][:corse[0].index(" (")]
	except ValueError:
		limitata = corse[0]
	for i in corse[1:]:
		if abs(convertiorario(hour) - convertiorario(i)) <= timedelta:
			return limitata
	return ""


def convertiorario(orario):
	h = orario.split(":")
	return int(h[0])*60 + int(h[1])


direzioni = linee.direzioni()
fermate = fermata()  # Chiave: Nome fermata, Valore: Numero fermata
busstop = bsnum()  # Chiave: Numero fermata, Valore: Codice fermata
codstop = inverti()  # Chiave: Numero fermata, Valore: Nome fermata
errore = "<div style='color: red; font-size: 1.2em; margin-top: 1em;'>"
ctm =\
	"http://busfinder.greenshare.it/InfoMobile/InfoBus/gateway?service=%7B%22name%22:%22previsioneTransiti%22," \
	"%22params%22:[%7B%22key%22:%22codicePalina%22,%22value%22:%22"
ctmf = "%22%7D]%7D"

rome = timezone("Europe/Rome")


with open(beta("path") + "parole.txt") as words:
	parole = eval(words.read())

with open(beta("path") + "passing.txt") as p:
	passing = eval(p.read())
	print passing
