# -*- coding: UTF-8 -*-

# Importa le librerie
import random  # Casuale
import re  # Espressioni regolari
import string  # Metodi sulle stringhe
import time  # Gestione del tempo
import urllib  # Gestione pagine web
import threading  # Programmazione multithread
import speech_recognition as sr  # Riconoscimento vocale
import telepot  # Libreria di Telegram
import subprocess  # Commandi di sistema, credo
import datetime
# from pydub import AudioSegment  # Conversione audio, necessita ffmpeg per funzionare
from telepot.namedtuple import ReplyKeyboardMarkup, ReplyKeyboardRemove, InlineKeyboardButton, InlineKeyboardMarkup
from os import path  # Indirizzi file e directory in locale

# Importa i file
import parsing
import scuse
import messaggi
import linee
import coordinate
import stats


class Statistica(threading.Thread):
	def __init__(self):
		super(Statistica, self).__init__()

	def run(self, flag=0):
		while 1:
			if time.strftime("%H") == "23" or flag:  # A fine giornata, manda un messaggio di statistiche
				mess = str(stats.statistiche())
				if datetime.datetime.strftime(datetime.datetime.today(), "%d") == "01" or flag:
					mess += "\n\n" + str(stats.month())
				try:
					bot.sendMessage(debugger[0], mess, parse_mode='HTML')
				except Exception as e:
					bot.sendMessage(debugger[0], "ERRORE STATISTICHE: " + str(e), parse_mode='HTML')
				if flag:
					break
				time.sleep(10000)
			time.sleep(3000)


class Update(threading.Thread):
	def __init__(self, chatid, msg, mid, kb):
		super(Update, self).__init__()
		self.s = -1
		self.lock = threading.RLock()
		self.chatid = chatid
		self.messaggio = msg
		self.mid = mid
		self.loc = ""
		self.f = 0
		self.kb = kb

	def set_time(self, s):
		with self.lock:
			self.s = s

	def set_msg(self, msg):
		with self.lock:
			self.messaggio = msg

	def set_loc(self, loc, fer):
		with self.lock:
			self.loc = loc
			self.f = fer
			print self.f

	def run(self):
		while self.s:
			if 1 < self.s < 100:
				with self.lock:
					invia = self.messaggio + messaggi.numeri(str(self.s))
					if self.s != 1:
						invia += " minuti."
					else:
						invia += " minuto."
					if self.loc:
						if self.f:
							invia += "\n\nSi trova a "
							if self.f != 1:
								invia += messaggi.numeri(str(self.f)) + " fermate "
							else:
								invia += "una sola fermata "
							invia += "da te.\n*"
						else:
							invia += "\n\nDeve partire dal capolinea.\n*"
						invia += self.loc + "*"
					try:
						bot.editMessageText(self.mid, invia, parse_mode="Markdown", reply_markup=self.kb)
					except telepot.exception.TelegramError as e:
						if "not modif" in str(e) or str(e) == messaggi.nonmodificato:
							pass
						else:
							print str(e) + "\nHo terminato."
							break
					except Exception as e:
						bot.sendMessage(canale, str(self.mid) + str(e))
					# except TypeError as e:
					# 	print str(e)
			time.sleep(5)


class Passing(threading.Thread):  # THREAD DI AGGIORNAMENTO FERMATE
	def __init__(self, chat_id, request, m_id):
		super(Passing, self).__init__()
		self.chat_id = chat_id
		self.request, self.passOld = request  # (Numero della fermata che ha cercato, Linea), Passaggio
		self.id = m_id  # Messaggio da modificare
		self.mess = messaggi.minuti(self.chat_id)
		self.kb = InlineKeyboardMarkup(inline_keyboard=[
			[InlineKeyboardButton(text="Ho cambiato idea.", callback_data="(\"0\",\"0\")")]])
		# Questi 5 None di fila sono solo per non farmi rompere le palle da Pycharm
		self.direzione = None  # Direzione della linea
		self.min = None  # Minuti di preavviso scelto dall'utente
		self.linea = None  # Lista con le fermate della linea
		self.fermata = None  # Indice della fermata nella lista della linea.
		self.lista = None  # (Fermata, [Linea, Orario]**)
		self.attesa = "0"  # Minuti di attesa alla fermata
		# I primi 4 attributi vengono definiti da questi due metodi
		self.minuti()
		self.direz()

	def direz(self):  # Questo metodo serve per capire in quale direzione è la fermata richiesta
		# Le linee hanno due direzioni: "As" è andata e "Di" è ritorno
		# Le circolari invece hanno una sola direzione "As"
		try:
			linea = linee.LINEA[self.request[1]]
		except KeyError:
			linea = self.request[1]
		print linea
		if int(self.request[0]) in linee.percorso(linea + ";As", True):
			self.direzione = linee.LINEA[self.request[1]] + ";As"
		elif int(self.request[0]) in linee.percorso(linea + ";Di", True):
			self.direzione = linee.LINEA[self.request[1]] + ";Di"
		else:
			self.direzione = ""
		self.linea = linee.percorso(self.direzione, True)
		try:
			self.fermata = self.linea.index(int(self.request[0]))
		except ValueError:
			self.fermata = None

	def minuti(self):  # Questo metodo legge la preferenza dell'utente circa i minuti di preavviso
		try:
			m = open("set" + str(self.chat_id) + ".txt")  # Dentro questo file c'è la preferenza dell'utente
		except Exception as e:
			print "File non aperto: " + str(e)  # Se il file non esiste, si imposta l'opzione di default
			self.min = 2  # Il default è 2 minuti
		else:
			self.min = int(m.read())
			m.close()

	def run(self):
		if not self.request or self.request == (0, 0):  # Se è vera questa condizione, è stata annullata la richiesta
			return
		# log = ""
		if 1:
			time.sleep(1)
			aggiorna = Update(self.chat_id, self.mess, self.id, self.kb)
			aggiorna.start()
			for j in range(60):  # Cicla al massimo di 60 volte, per evitare che il thread rimanga aperto troppo a lungo
				sec = parsing.quantomanca(self.request)
				print sec
				if sec:
					try:
						bot.editMessageText(self.id, "CagliariBusBot\n" + messaggi.attesa())
					except Exception as e:
						if "not found" in str(e):
							break
					self.attesa = sec[1]
					aggiorna.set_time(sec[1])
					self.mess = "Linea " + self.request[1] + "       `" + sec[0] + "`\nPassa tra "
					aggiorna.set_msg(self.mess)
					print sec[1]
					if sec[1] <= self.min or sec[1] > 1400:
						# LA LINEA È IN PASSAGGIO
						messaggio = "Hey, "
						messaggio += parsing.find_between(nomi[self.chat_id], "", " ") + "! "
						if self.request[1].upper() == "U-EX":
							invia = "University Express"
						else:
							invia = self.request[1]
						if invia in ["1", "8", "M", "1Q", "8A", "8G", "11", "University Express"]:
							messaggio += "L'"
						else:
							messaggio += "Il "
						messaggio += invia + " sta per passare alla fermata "
						messaggio += parsing.correggiascii(parsing.codstop[int(self.request[0])] + ".")
						messaggio += "\nPrevisione di passaggio: `" + str(sec[0]) + "`"
						bot.deleteMessage(self.id)
						manda([messaggio, []], self.chat_id, "MESSAGGIO AUTOMATICO", time.clock())
						aggiorna.set_time(1)
						return
					try:
						fer, loc = self.localizza()
					except KeyError:
						pass
					else:
						aggiorna.set_loc(linee.percorso(self.direzione)[loc][0], fer)
					time.sleep(sec[1] * 4 or 20)

	def localizza(self):
		if self.direzione:
			attesa = int(self.attesa)
			for j in range(self.fermata - attesa / 2, 0, -1):
				lista = parsing.separa_messaggio(ricerca(parsing.busstop[self.linea[j]])[0])
				# print nomi[self.chat_id], lista
				linea = []  # Linee in questa fermata
				# print lista
				for i in range(1, len(lista)):
					linea.append(lista[i][0])
					if lista[i][0] == self.request[1]:
						appoggio = int(aggiungi60(int(lista[i][1][3:])))
						# print "Attesa", attesa, appoggio
						# print attesa < appoggio
						if attesa < appoggio:
							# print "Sono dentro l'if"
							return self.fermata - j - 1, j + 1
						else:
							attesa = appoggio
				if self.request[1] not in linea:  # Se la linea non c'è, probabilmente è già passata
					return self.fermata - j, j
			return 0, 0
		else:
			return


class Loading(threading.Thread):
	def __init__(self, chat_id, m_id, richiesta):
		super(Loading, self).__init__()  # Richiama il costruttore nella sopraclasse Thread
		self.chat_id, self.m_id, self.request = chat_id, m_id, richiesta
		self.lock = threading.RLock()
		self.caricamento = messaggi.attesa()

	def fermata(self, mess):
		with self.lock:
			self.caricamento = mess

	def run(self):
		# caricamento = messaggi.attesa()
		# print caricamento
		i = 0
		caricamento = self.caricamento
		print caricamento
		while 1:
			caricamento += "."
			time.sleep(1)
			caricamento = caricamento.replace("....", "")
			i += 1
			if i == 10:
				caricamento = "La connessione al server sta richiedendo più tempo del previsto."
			elif i == 20:
				caricamento = "Sono mortificato, ma il server non risponde.\nSto ancora provando a entrare."
			elif i == 30:
				caricamento = "Sto facendo del mio meglio, te lo giuro.\nAncora nulla."
			elif i == 40:
				caricamento = "Sicuramente sei in ritardo, vero?\nAspetta e spera"
			elif i == 50:
				caricamento = "Guarda, vai a piedi."
			try:
				bot.editMessageText((self.chat_id, self.m_id), caricamento)
			except telepot.exception.TelegramError as e:
				if str(e) == messaggi.nonmodificato:
					pass
				else:
					break


class Handle(threading.Thread):
	def __init__(self, msg):
		super(Handle, self).__init__()
		self.msg = msg
		self.content_type, self.chat_type, self.chat_id = telepot.glance(self.msg)  # Acquisizione dati della chat
		self.caricamento = "Attendi il caricamento degli orari."
		self.m_id = 0

	def run(self, i=0, mid=""):
		print i
		t0 = time.clock()  # Tempo del processore all'inizio del processo
		if i > 5:
			manda("Ho riprovato troppe volte. Mi spiace.", self.chat_id, self.msg['txt'], t0, mid)
			return
		if self.chat_id == canale and self.content_type == "text":  # Se abbiamo scritto nel canale, ...
			rispondi(self.msg['text'])  # ... inoltra il messaggio al destinatario scelto
		elif self.content_type == "text" and self.chat_type != "channel":  # Caso: Testo, non canale
			self.m_id = bot.sendMessage(self.chat_id, self.caricamento)["message_id"]
			txt = parsing.correzioni(self.msg['text'])
			if self.chat_type == "group":
				if "@cagliaribusbot " in txt and "@" == txt[0]:  # Accetta solo se il comando inizia con il nick
					txt = string.replace(txt, "@cagliaribusbot ", "")
				elif self.chat_id in mensa:
					if "0" < time.strftime("%w") < "6" and txt.lower() in ["mensa?", "palazzo?"]:
						bot.sendMessage(self.chat_id, "Scusate, " + random.choice(scuse.scusa) + "Ora cerco gli orari.")
						bot.sendChatAction(self.chat_id, "typing")
						manda([scuse.easteregg(txt), []], self.chat_id, self.msg["text"], t0)
					txt = "204"
				else:
					txt = ""
			if not txt:
				return
			elif txt == "/start":  # Messaggio di saluto
				print "start"
				manda(messaggi.saluto, self.chat_id, txt, t0, self.m_id)
				return
			elif "/help" == txt:  # Messaggio di aiuto
				bot.deleteMessage((self.chat_id, self.m_id))
				aiuto(self.chat_id)
				return
			elif "/stats" == txt and self.chat_id == debugger[0]:
				bot.deleteMessage((self.chat_id, self.m_id))
				sta.run(1)
				return
			elif "/passing" == txt:
				if parsing.passoff(self.chat_id):
					manda(["Funzionalità \"Passing\" riattivata.", []], self.chat_id, txt, t0, self.m_id)
				else:
					manda(["Funzionalità \"Passing\" disattivata.", []], self.chat_id, txt, t0, self.m_id)
			elif txt == "/changelog":
				bot.deleteMessage((self.chat_id, self.m_id))
				kb = [[InlineKeyboardButton(text="Changelog", url="https://t.me/joinchat/AAAAAE4IuML5H57Nc0qS9Q")]]
				kb = InlineKeyboardMarkup(inline_keyboard=kb)
				bot.sendMessage(self.chat_id, "Guarda le nuove funzionalità qui:", reply_markup=kb)
			elif "/" == txt[0]:  # Messaggio per chi non ha ancora capito come funziona il bot
				manda(messaggi.slash, self.chat_id, txt, t0, self.m_id)
				return
			elif messaggi.sql_controllo(txt):
				manda(messaggi.sql, self.chat_id, txt, t0, self.m_id)  # Messaggio contro le SQL injection
				return
			else:
				richiesta = self.parsa(txt)
				if not richiesta:
					bot.deleteMessage((self.chat_id, self.m_id))
					return
				messaggio = self.cerca(richiesta)
				if "La connessione ha impiegato troppo tempo." in messaggio[0]:
					self.run(i + 1, self.m_id)
				else:
					bot.deleteMessage((self.chat_id, self.m_id))
					manda(messaggio, self.chat_id, self.msg["text"], t0)
		elif self.chat_type == "private" and self.content_type in ["voice", "video_note"]:  # Ricerca vocale
			# Scarica l'audio per poterci lavorare
			print parsing.beta("path") + str(self.chat_id) + ".ogg"
			try:
				voce = self.msg["voice"]
				ext = ".ogg"
			except KeyError:
				voce = self.msg["video_note"]
				bot.sendMessage(self.chat_id, messaggi.choice(messaggi.video))
				ext = ".mp4"
			file_audio = parsing.beta("path") + str(self.chat_id) + ext
			bot.download_file(voce["file_id"], file_audio)
			command = self.vocale(file_audio)
			subprocess.call(command, shell=True)
			# sound = AudioSegment.from_ogg(parsing.beta("path") + str(self.chat_id) + ".ogg")  # In sound c'e' l'audio
			# sound.export(parsing.beta("path") + str(self.chat_id) + ".wav", format="wav")  # Conversione in wav
			audio = path.join(path.dirname(path.realpath(__file__)), str(self.chat_id)+".wav")  # Apertura file wav
			# L'audio e' convertito perche' le API per lo speech to text utilizzano solo file wav
			# Questo non so bene che fa ma presumo prepari il file per essere elaborato
			r = sr.Recognizer()
			with sr.AudioFile(audio) as source:
				audio = r.record(source)
			try:  # Prova a...
				testo = r.recognize_google(audio, language="it-IT")  # ... scrivere quanto detto nel vocale
			except Exception as e:  # Se non capisce...
				print str(e)
				manda(messaggi.nonhocapito, self.chat_id, self.content_type, t0)  # ... manda un messaggio d'errore
			else:  # Altrimenti, (in caso di successo)
				manda(["Sto cercando:\n" + testo, []], self.chat_id, self.content_type, t0)  # Manda all'utente cosa ha capito
				t0 = time.clock()  # Considera il tempo impiegato dal momento in cui inizia a cerca e poi manda il risultato
				txt = parsing.correzioni(testo)
				messaggio = self.cerca(self.parsa(txt))
				# bot.deleteMessage((self.chat_id, self.m_id))
				manda(messaggio, self.chat_id, txt, t0)
		elif self.content_type == "location" and self.chat_type != "group":
			# 0.001° sono circa 111 metri
			posizione = coordinate.tupla(self.msg["location"])
			pos, dist = coordinate.cercavicine(posizione)
			messaggio = "*Queste sono le fermate più vicine a te:* \n\n"
			for i in range(len(pos)):
				messaggio += pos[i][0] + "\n_" + str(int(dist[i])) + " metri_\n\n"
			manda([messaggio, pos], self.chat_id, str(posizione), time.clock())
		else:  # Se non ha mandato un testo né un audio...
			if self.chat_type == "private":  # ... e la chat e' privata...
				manda(messaggi.nonvalido, self.chat_id, self.content_type, t0)  # ... allora la richiesta non e' valida

	def vocale(self, file_audio):
		c = "ffmpeg -y -i " + file_audio + " -ab 160k -ac 2 -ar 44100 -vn " + \
			parsing.beta("path") + str(self.chat_id) + ".wav"
		return c

	def cerca(self, richiesta):
		attendi = Loading(self.chat_id, self.m_id, richiesta)
		# print parsing.codstop  # parsing.codstop(int(richiesta[2:]))
		try:
			attendi.caricamento = "Sto caricando gli orari per la fermata " + parsing.codstop[int(richiesta[2:])]
		except (KeyError, ValueError):
			attendi.caricamento = messaggi.attesa()
		finally:
			bot.sendChatAction(self.chat_id, "typing")
			attendi.start()
			messaggio = ricerca(richiesta)
		# print(threading.enumerate())
		return messaggio

	def parsa(self, txt):
		richiesta = parsing.stop(txt)  # Quindi salva su "richiesta" il codice della fermata
		if type(richiesta) == list:
			manda(["Scegli la fermata:", richiesta], self.chat_id, txt, time.clock())
			return None
		else:
			return richiesta


def aiuto(chat_id, root="0", mid=0):
	try:
		ind = int(root)
	except ValueError:
		bot.editMessageText((chat_id, mid), messaggi.aiuto[-1], parse_mode="Markdown")
	else:
		kb = InlineKeyboardMarkup(inline_keyboard=keyboard(ind))
		if not mid:
			bot.sendMessage(chat_id, messaggi.aiuto[ind], parse_mode="Markdown", reply_markup=kb)
		else:
			bot.editMessageText((chat_id, mid), messaggi.aiuto[ind], parse_mode="Markdown", reply_markup=kb)


def keyboard(root):
	kb = [[]]
	puls = messaggi.pulsanti_aiuto
	kb[0].append(InlineKeyboardButton(text=puls[1], callback_data=str(int(root - 1) % (len(messaggi.aiuto) - 1))))
	kb[0].append(InlineKeyboardButton(text=puls[0], callback_data=str(int(root + 1) % (len(messaggi.aiuto) - 1))))
	kb.append([InlineKeyboardButton(text="Informativa sulla privacy", callback_data="privacy")])
	return kb


def aggiungi60(a):
	attesa = a - int(time.strftime("%M"))
	if attesa < -5:
		attesa += 60
	return str(attesa)


def ricerca(richiesta):
	return parsing.cerca(urllib.urlopen(ctm + richiesta + ctmf)) if richiesta else False


def onchatmessage(chat_id, lista, req):
	kb = []
	if len(lista) == 1:
		return
	for i in range(1, len(lista)):
		cb = "(\"" + req + "\",\"" + lista[i][0] + "\"), " + "\"" + lista[i][1] + "\""
		if lista[i][1]:
			kb.append([InlineKeyboardButton(text="[ " + lista[i][0] + " ]  " + lista[i][1], callback_data=cb)])
	kb = parsing.dividilista(kb)
	kb.append([InlineKeyboardButton(text="Lascia perdere", callback_data="(\"0\",\"0\")")])
	kb = InlineKeyboardMarkup(inline_keyboard=kb)
	time.sleep(1)
	bot.sendMessage(chat_id, messaggi.aspettando, reply_markup=kb)


def oncallback(msg):
	query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
	print query_data
	if "lt" in query_data:
		query_data = query_data.replace("lt", "")
		query_data = eval(query_data)
		manda(["Scegli la fermata: ", coordinate.cercavicine(query_data)[0]], from_id, "VICINE", time.clock())
	elif ";" in query_data:
		bot.deleteMessage((from_id, msg["message"]["message_id"]))
		kb = linee.percorso(query_data)
		manda(["Scegli la fermata.", kb], from_id, query_data, time.clock())
	elif query_data == "privacy" or "0" <= query_data < "9":
		aiuto(from_id, query_data, msg["message"]["message_id"])
	else:
		if query_data in ["null", "(\"0\",\"0\")"]:
			bot.sendMessage(from_id, random.choice(scuse.antipatico))
			bot.sendMessage(canale, "Passing annullato.\n" + str(from_id))
			bot.deleteMessage((from_id, msg["message"]["message_id"]))
		else:
			try:
				passaggi = Passing(from_id, eval(query_data), msg["message"]["message_id"])
				bot.sendMessage(canale, "Passing: " + str(from_id) + "\n" + str(query_data))
			except Exception as e:
				bot.editMessageText((from_id, msg["message"]["message_id"]), "Mi spiace, qualcosa è andato storto, arrangiati.")
				bot.sendMessage(canale, "On call back:\n " + str(query_data) + "\n" + str(e))
				return
			else:
				passaggi.id = (from_id, msg["message"]["message_id"])
				passaggi.start()
				bot.answerCallbackQuery(query_id, "Fidaty che ti avviso!")
				bot.editMessageText((from_id, msg["message"]["message_id"]), messaggi.minuti(from_id), reply_markup=passaggi.kb)


def manda(invio, chat_id, txt, t0, m_id=None):  # Funzione per l'invio dei messaggi
	kb = ReplyKeyboardRemove()
	mid = bot.sendMessage(chat_id, "Questo è un messaggio subliminale.", reply_markup=kb)['message_id']
	bot.deleteMessage((chat_id, mid))
	tastiera = invio[1]
	# print tastiera
	if m_id:
		bot.deleteMessage((chat_id, m_id))  # Elimina il messaggio di caricamento
	if time.clock() - t0 > 0.08:  # Se ci ha impiegato troppo la colpa e' dei server.
		text = messaggi.server
	else:  # Altrimenti...
		text = ""  # ... inizializza la stringa di risposta.
	try:
		if "Hey" not in invio[0]:
			with open(parsing.beta("path") + "annuncio.txt") as a:
				text += a.read() + "\n\n"  # Annunci vari
	except IOError:
		pass
	text += invio[0]
	num = parsing.find_between(text, "Numero fermata: ", ".")
	if chat_id in parsing.passing and num:
		text += "\nRiattiva il Passing con il commando /passing."
	# Risposta da mandare
	if parsing.umano(text):  # parsing.umano verifica se in "text" c'è scritto "nessuna fermata trovata"
		text = messaggi.umano(txt)
		# tastiera = invio[1]
		if text == txt:
			tastiera = []
			if "Nessuna" in invio[0]:
				tastiera = parsing.forse_cercavi(text)
				if tastiera:
					text = invio[0] + "\n\nForse cercavi:"
				else:
					text = invio[0] + messaggi.posizione
			else:
				for i in fermate.keys():
					if text.lower() in i:
						print i
						if len(tastiera) < 20:
							tastiera.append([i.upper()])
						else:
							break
				if tastiera:
					text = messaggi.troppefermate
				else:
					text = invio[0]
		if tastiera:
			tastiera = ReplyKeyboardMarkup(keyboard=tastiera[:50], one_time_keyboard=True)
		else:
			tastiera = ReplyKeyboardRemove()
	elif len(invio[1]):  # Composizione della tastiera
		if "Riprova" in invio[0]:
			tastiera = ReplyKeyboardRemove()
			bot.sendMessage(canale, str(invio[1]))
		if chat_id > 0:
			tastiera = invio[1]  # Array dei pulsanti
			tastiera = ReplyKeyboardMarkup(keyboard=tastiera, one_time_keyboard=True)
	else:  # Altrimenti, se non ci sono pulsanti...
		if num:
			bs = parsing.find_between(invio[0], "*", "*")
			bs = parsing.busstop[int(fermate[parsing.correggiascii(bs.lower())])]
			try:
				coord = "lt" + str(coordinate.mappa2[bs])
			except KeyError as e:
				bot.sendMessage(canale, "Mancano le coordinate! " + str(e))
				coord = ""
				tastiera = ReplyKeyboardRemove()
			else:
				tastiera = [[InlineKeyboardButton(text=messaggi.fermatevicine, callback_data=coord)]]
				tastiera = InlineKeyboardMarkup(inline_keyboard=tastiera)
		else:
			tastiera = ReplyKeyboardRemove()  # ... la tastiera viene rimossa
	# print tastiera
	# Preparazione per l'inoltro al canale di debug
	inoltro = " (" + str(chat_id) + "):\n" + txt + "\n\nRisposta: \n" + parsing.correggiascii(text)
	if len(invio[1]) != 0:  # Se ci sono pulsanti...
		inoltro += "\n\nTastiera:\n" + str(invio[1])  # ... allora vengono mandati al canale
	inoltro += "\n\nTempo: " + str(time.clock() - t0)  # Debug
	inoltro = inoltro[:4000]
	if "Hey" in inoltro:
		inoltro = inoltro.replace(parsing.find_between(inoltro, "Hey, ", "!"), "***")
	try:
		if tastiera:
			bot.sendMessage(chat_id, text, parse_mode="Markdown", reply_markup=tastiera)  # Invio con tastiera
		else:
			bot.sendMessage(chat_id, text, parse_mode="Markdown")
	except Exception as e:
		if "too long" in str(e):
			bot.sendMessage(chat_id, "Troppe fermate, restringi la ricerca.")
		else:
			print str(e)
			print text
			bot.sendMessage(canale, str(chat_id) + str(e))
	else:
		bot.sendMessage(canale, inoltro)  # Invio dell'inoltro al canale di debug
	if num:
		if chat_id not in parsing.passing:
			onchatmessage(chat_id, parsing.separa_messaggio(text), num)  # Chiede all'utente cosa sta aspettando
		if parsing.beta("path"):
			stats.inserisci("log", chat_id, txt, str(num))


def rispondi(testo):  # Funzione per mandare messaggi all'utente
	if "MID" in testo:
		try:
			messaggio = eval(testo[4:])
			bot.forwardMessage(canale, messaggio[0], messaggio[1])
		except Exception as e:
			bot.sendMessage(canale, str(e))
	else:
		chat_id = int(re.findall("^-*[0-9]*", testo)[0])  # Considera l'IDChat dell'utente
		if not(chat_id in nomi.keys()):  # Evita di generare errori inutili
			nomi[chat_id] = "Rip nome"
			username[chat_id] = "Rip username"
		manda([testo.replace(str(chat_id), ""), []], chat_id, "*ADMIN*\n" + testo, time.clock())  # Cancella l'id e manda


def identifica(msg, chat_id):
	try:
		nome = msg["from"]["first_name"] + " "
	except KeyError:
		nome = "Nessun nome?"
	try:
		cognome = msg["from"]["last_name"]
	except KeyError:
		cognome = ""
	nomi[chat_id] = nome + cognome
	try:
		username[chat_id] = msg["from"]["username"]
		if chat_id in debugger or chat_id < 0:  # Questa condizione e' per evitare che il bot ci taggasse a ogni richiesta
			username[chat_id] = " " + username[chat_id]
	except KeyError:
		username[chat_id] = ""
	try:  # STATISTICHE
		if messaggi.sql_controllo(nomi[chat_id]):
			bot.sendMessage(chat_id, "Sei sicuro di chiamarti davvero così? Se cerchi di fare una SQL injection, caschi male!")
		elif chat_id > 0:
			stats.inserisci("utenti", chat_id, nome, cognome, username[chat_id].replace(" ", ""))
	except Exception as e:
		bot.sendMessage(debugger[0], str(e))


def aprithread(msg):
	content_type, chat_type, chat_id = telepot.glance(msg)
	if chat_id == -1001181013075:
		return
	identifica(msg, chat_id)
	if chat_id > 0:  # Se la chat e' positiva, e' privata (Evita che inoltri tutti i messaggi dei gruppi), allora ...
		testo = "`MID: (" + str(chat_id) + ", " + str(msg["message_id"]) + ")`\n"
		try:
			testo += msg['text']
		except KeyError:
			testo += content_type
			testo = testo.replace("_", "-")
		try:
			bot.sendMessage(canale, testo, parse_mode="Markdown")  # ... inoltra il messaggio nel canale del debug
		except telepot.exception:
			bot.sendMessage(canale, testo)
	if content_type == "text":
		if msg["text"].lower() == "africa":
			bot.sendMessage(chat_id, "I bless the rain down in")
		if "/set" in msg["text"] and "/" == msg["text"][0]:
			text = msg["text"].replace("/set ", "")
			if chat_id > 0:
				try:
					minuti = int(text)
					if minuti < 0:
						manda([random.choice(scuse.simpatia), []], chat_id, msg["text"], time.clock())
						return
				except Exception as e:
					print e
					manda(messaggi.set_errato, chat_id, msg["text"], time.clock())
				else:
					print "Sono nell'else"
					with open("set" + str(chat_id) + ".txt", "w") as setmin:
						setmin.write(str(minuti))
					if minuti > 15:
						manda(["Se vuoi ti avviso direttamente il giorno prima.", []], chat_id, msg["text"], time.clock())
					elif minuti == 0:
						manda(["Ok, ti avviso quando ce l'hai di fronte", []], chat_id, msg["text"], time.clock())
					elif minuti < 0:
						manda(["Ok, ti avviso quando è già passato.", []], chat_id, msg["text"], time.clock())
					else:
						print "0 ~ 15"
						manda(["Perfetto, verrai avvisato tempestivamente.", []], chat_id, msg["text"], time.clock())
				finally:
					return
			elif chat_id == canale:
				with open(parsing.beta("path") + "annuncio.txt", "w") as a:
					a.write(msg["text"][5:])
		elif linee.regex(msg["text"]) or "university" in msg["text"].lower() and chat_id > 0:
			print "Ha cercato una linea."
			text = msg["text"].upper()
			text = text.replace("LINEA ", "")
			text = text.replace("CIRCOLARE ", "")
			print text
			try:
				c = linee.LINEA[text]
			except Exception as e:
				print str(e)
				manda(messaggi.cerca_linea, chat_id, msg['text'], time.clock())
			else:
				print c
				try:
					direzione = direzioni[c + ";As"]
				except KeyError:
					manda([messaggi.fuoristagione, messaggi.linee], chat_id, msg['text'], time.clock())
					return
				print direzione
				kb = [[InlineKeyboardButton(text=direzione, callback_data=c + ";As")]]
				print kb
				try:
					direzione = direzioni[c + ";Di"]
				except Exception as e:
					print str(e)
				else:
					print kb
					kb.append([InlineKeyboardButton(text=direzione, callback_data=c + ";Di")])
				kb = InlineKeyboardMarkup(inline_keyboard=kb)
				bot.sendMessage(chat_id, "Direzione?", reply_markup=kb)
			return
	if chat_id not in chat.items():
		chat[chat_id] = Handle(msg)
	try:
		chat[chat_id].start()
	except Exception as e:
		print str(e)
		manda(["Puoi fare una sola richiesta, mi spiace.", [msg["text"]]], chat_id, msg["text"], time.clock())


nomi = {}  # Chiave: chat_id, Valore: Nome e cognome
username = {}  # Chiave: chat_id, Valore: Username
chat = {}  # Chiave: chat_id, Valore: thread
# ctm = "http://m.ctmcagliari.it/fermate.php?s="
ctm = parsing.ctm
ctmf = parsing.ctmf

canale = parsing.beta("canale")
fermate = parsing.fermate  # Restituisce un dizionario. Chiave: nome fermata, Valore: codice fermata
direzioni = parsing.direzioni  # Restituisce due dizionari, capolinea - direzione.

mensa = [-175696916, -266377060]
debugger = [184959894, 8738465]

TOKEN = parsing.beta("token")
bot = telepot.Bot(TOKEN)
sta = Statistica()
sta.start()

if not stats.cur:
	bot.sendMessage(184959894, "DATABASE NON ATTIVO", disable_notification=True)

bot.sendMessage(-1001181013075, "CagliariBus Online", disable_notification=True)

bot.message_loop({"chat": aprithread, "callback_query": oncallback})
print "Attivo"

while 1:
	time.sleep(10)
