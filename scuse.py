# -*- coding: UTF-8 -*-
import parsing
import urllib
import string


def mensa():  # Personalizzazioni
	orari = open("mensa.txt", "r")
	messaggio = "*Previsioni di passaggio per andare in mensa:*\n\n"
	while 1:
		linea = orari.readline()
		if linea == "":
			orari.close()
			print messaggio
			return messaggio
		elif "Linea 7" in linea:
			messaggio += "Linea 7 dalla Clinica Macciotta:\t" + orari.readline() + "\n"
		elif "Linea 6" in linea:
			messaggio += "Linea 6 dal terrapieno:\t" + orari.readline() + "\n"


def palazzo():  # Personalizzazioni
	orari = open("palazzo.txt", "r")
	messaggio = "Per tornare: \n"
	flag = 0
	while 1:
		linea = orari.readline()
		if linea == "":
			orari.close()
			return messaggio
		elif "Brotzu" in linea:
			if flag == 0:
				messaggio += "\n*Opzione 1+10*\nLinea 1 dalla concessionaria: "+orari.readline()
				flag = 1
			else:
				messaggio += "L'1 arriva in via Sonnino alle "+orari.readline()
		elif "Linea 10" in linea:
			messaggio += "Il 10 passa alle "+orari.readline()
		elif "Linea 6" in linea:
			messaggio += "\n*Opzione 6*\nLinea 6 dalla concessionaria: "+orari.readline()


def easteregg(txt):
	if "mensa?" == string.lower(txt):
		ospedale, tastiera = parsing.cerca(urllib.urlopen("http://m.ctmcagliari.it/fermate.php?s=60"))
		terrapieno, tastiera = parsing.cerca(urllib.urlopen("http://m.ctmcagliari.it/fermate.php?s=66"))
		f = open("mensa.txt", "w")
		f.write(ospedale + "\n" + terrapieno)
		f.close()
		return mensa()
	elif "palazzo?" == string.lower(txt):
		sei, tastiera = parsing.cerca(urllib.urlopen("http://m.ctmcagliari.it/fermate.php?s=204"))
		dieci, tastiera = parsing.cerca(urllib.urlopen("http://m.ctmcagliari.it/fermate.php?s=20"))
		uno, tastiera = parsing.cerca(urllib.urlopen("http://m.ctmcagliari.it/fermate.php?s=205"))
		f = open("palazzo.txt", "w")
		f.write(uno + "\n" + dieci + "\n" + sei)
		f.close()
		return palazzo()


scusa = [
	"stavo guardando dove sono i controllori. ",
	"stavo dicendo agli altri di non usare BusFinder. ",
	"stavo cambiando una gomma al 6. ",
	"stavo girando una sigaretta. ",
	"stavo triangolarizzando laggente. ",
	"stavo fumando. ",
	"stavo studiando. ",
	'stavo fissando un punto nel vuoto per vedere se gli altri si girano. ',
	'mi stavo facendo cambiare i soldi da Basilio. ',
	'mi sono perso negli occhi di una tipa. ',
	"non pensavo voleste già andare via. ",
	"pensavo voleste fare la muffa. ",
	'stavo guardando quanti pasti avessi. ',
	'stavo salutando. ',
	'facevo cose. ',
	'ho visto Laura Cartone e ho dovuto cambiare mezzo. ',
	'stavo scrivendo le dispense dei mezzi pubblici. ',
	'mi stavo girando i pollici. ',
	'mi stavo lamentando. ',
	"non c'è campo. ",
	"ero affascinato dai vostri discorsi. ",
	"stavo ricaricando i pasti. ",
	"guardavo le GIF del gruppo. ",
	"hackeravo esse3. ",
	"vi stavo ignorando. ",
	"non ne ho voglia. ",
	"stavo cercando un prodotto vettoriale su R^4. ",
	"stavo dicendo all'HTML che non Ã¨ un linguaggio di programmazione. ",
	"stavo orientando nastri di Moebius. ",
	"stavo guardando come procedono i lavori a Palazzo. ",
	"ma perché non andate al Magistero? ",
	"ma perché non rimanete ancora un po'? ",
	"stavo andando a riempire la brocca. "]

antipatico = [
	"Ok, sarà per la prossima volta.",
	"Va bene.",
	"Volevo solo esserti utile.",
	"Okay.",
	"Sicuro sicuro? Pensaci, dai."]

simpatia = [
	"Ah se non vuoi essere avvisato, basta dirlo.",
	"Aspetta che mi devo riprendere dalle risate.",
	"Ha fatto la battuta.",
	"Sì, ho messo il controllo per i numeri negativi."]