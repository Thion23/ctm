# -*- coding: UTF-8 -*-

import psycopg2
import messaggi
import datetime
from parsing import correggiascii, beta


def execute(query):
	if cur:
		cur.execute(query)
		db.commit()


def inserisci(tabella, *args):
	if not cur:
		return
	argomenti = ""
	for i in args:
		if type(i) == int:
			argomenti += str(i) + ", "
		else:
			if len(i) >= 100:
				i = i[:99]
			argomenti += "'" + i.replace("'", "''") + "', "
		argomenti = correggiascii(argomenti.replace(", ''", ", NULL"))
	if tabella == "utenti":
		table = "utenti"
		now = ""
		cur.execute(utenti)
		row = cur.fetchall()
		for i in row:
			if i[0] in args:
				execute(agg(args))
				return
	else:
		table = "log (chatid, domanda, risposta, giorno)"
		now = ", 'now'"
	execute("INSERT INTO ctm." + table + " VALUES (" + argomenti[:-2] + now + ")")


def connect():
	h = "awsmtrcl.cp22h9doic2q.eu-central-1.rds.amazonaws.com"
	try:
		with open("/home/ubuntu/pw") as acc:
			pw = acc.read()
	except IOError:
		with open("/home/thion/pw") as acc:
			pw = acc.read()
	try:
		# raise ZeroDivisionError
		database = psycopg2.connect(dbname="postgres", host=h, user="AWServer", password=pw, port=2302)
	except psycopg2.DatabaseError:
		cursor = None
		database = None
	else:
		cursor = database.cursor()
	return cursor, database
#	return "", ""


def statistiche():
	if cur:
		cur.execute(oggi)
		row = cur.fetchall()
		mess = "Oggi, "
		if len(row) == 0:
			mess += "nessuno mi ha usato."
		if len(row) == 1:
			mess += "solo una persona mi ha usato."
		if len(row) > 1:
			mess += "mi hanno usato " + messaggi.numeri(len(row)) + " persone (<b>nuovi: "
			cur.execute(nuovi())
			new = cur.fetchall()
			mess += str(len(new)) + "</b>)"  # \n\nUtenti:\n"
		cur.execute(ferm)
		fermate = cur.fetchall()
		mess += "\nFermate cercate: " + messaggi.numeri(len(fermate)) + "\nRicerche: "
		s = 0
		lista_fermate = ""
		for i in fermate:
			if fermate.index(i) <= 3:
				lista_fermate += i[0] + " (" + str(i[1]) + ")\n"
			s += i[1]
		mess += messaggi.numeri(s) + "\n\n" + lista_fermate
	else:
		mess = "Mi spiace, il database è offline."
	return mess


def month():
	if cur:
		print mese()[0]
		cur.execute(mese()[0])
		row = cur.fetchall()
		mess = "In questo mese, "
		if len(row) == 0:
			mess += "nessuno mi ha usato."
		if len(row) == 1:
			mess += "solo una persona mi ha usato."
		if len(row) > 1:
			mess += "mi hanno usato " + messaggi.numeri(len(row)) + " persone."
		cur.execute(mese()[1])
		fermate = cur.fetchall()
		mess += "\nFermate cercate: " + messaggi.numeri(len(fermate)) + "\nRicerche: "
		s = 0
		lista_fermate = ""
		for i in fermate:
			if fermate.index(i) <= 3:
				lista_fermate += i[0] + " (" + str(i[1]) + ")\n"
			s += i[1]
		mess += messaggi.numeri(s) + "\n\n" + lista_fermate
	else:
		mess = "Mi spiace, il database è offline."
	return mess


def nuovi():
	with open(beta("path") + "confronto.sql") as f:
		query = f.read()
	return query


def utente(name):
	stringa = name[0]
	if name[1]:
		stringa += name[1]
	if name[2]:
		stringa += " (" + name[2] + ")"
	return stringa


def agg(args):
	query = " UPDATE ctm.utenti SET nome = '" + args[1] + "', cognome = "
	if args[2]:
		query += "'" + args[2] + "'"
	else:
		query += "NULL"
	query += ", nick = "
	if args[3]:
		query += "'" + args[3] + "'"
	else:
		query += "NULL"
	query += " WHERE chatid = " + str(args[0])
	return query


def mese():
	return [
		"select chatid, count(*) FROM ctm.log WHERE giorno::date BETWEEN " +
		datetime.datetime.strftime(datetime.datetime.today() - datetime.timedelta(1), "'%Y-%m-01' ") +
		"AND 'today' GROUP BY chatid ORDER BY count(*) DESC",
		"select nome, count(*) from ctm.fermate "
		"join ctm.log on risposta=num "
		"where giorno::date BETWEEN " +
		datetime.datetime.strftime(datetime.datetime.today() - datetime.timedelta(1), "'%Y-%m-01' ") +
		"AND 'today' "
		"group by nome "
		"order by count desc "
	]


print "Connessione al database..."
cur, db = connect()
print "Connesso"

# Query
utenti = "SELECT chatid FROM ctm.utenti"
oggi = "SELECT chatid, count(*) FROM ctm.log WHERE giorno::date = 'today' GROUP BY chatid ORDER BY count(*) DESC"

ferm = """select nome, count(*)
from ctm.fermate
join ctm.log on risposta=num
where giorno::date = 'today'
group by nome
order by count desc"""
